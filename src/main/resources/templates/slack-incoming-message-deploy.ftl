<#if executionData.job.group == "" >
    <#assign jobName="${executionData.job.name}">
    <#assign fullServiceName="${executionData.project}">
<#else>
    <#assign jobName="${executionData.job.group} / ${executionData.job.name}">
    <#assign fullServiceName="${executionData.project}-${executionData.job.group}">
</#if>
<#assign message="Deploy for <${executionData.job.href}|${fullServiceName}> started by ${executionData.user} has ended">
<#if trigger == "start">
    <#assign state="Started">
<#elseif trigger == "failure">
    <#assign state="Failed">
<#else>
    <#assign state="Succeeded">
</#if>

{
   "attachments":[
      {
         "fallback":"${state}: ${message}",
         "pretext":"${message}",
         "color":"${color}",
         "fields":[
            {
               "title":"Job Name",
               "value":"<${executionData.job.href}|${jobName}>",
               "short":true
            },
            {
               "title":"Project",
               "value":"${executionData.project}",
               "short":true
            },
            {
               "title":"Status",
               "value":"<${executionData.href}|${state}>",
               "short":true
            },
            {
               "title":"Deploy parameters",
               "value":"${executionData.argstring}",
               "short":true
            }
<#if trigger == "failure">
            ,{
               "title":"Failed Nodes",
               "value":"${executionData.failedNodeListString!"- (Job itself failed)"}",
               "short":false
            }
</#if>
]
      }
   ]
}

